refcard
=======

# Overview

Printable reference card for the Debian system

The Debian reference card provides new users help with the most important
commands. Basic knowledge of computers, files, directories and the command
line is required, however. The package contains printable PDF files in multiple
languages.

# Linked links

+ [refcard - Debian Website](https://www.debian.org/doc/manuals/refcard/)
+ [refcard - Debian Package Tracker](https://tracker.debian.org/pkg/refcard)
+ [refcard - Debian Packages](https://packages.debian.org/search?searchon=names&keywords=refcard)
+ [refcard - Debian Source Package](https://packages.debian.org/search?searchon=sourcenames&keywords=refcard)
+ [refcard - Debian Salsa repository](https://salsa.debian.org/ddp-team/refcard)
